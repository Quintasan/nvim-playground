FROM ubuntu:22.04
RUN apt update && apt install -y software-properties-common && add-apt-repository -y ppa:neovim-ppa/unstable && apt install -y neovim python3-dev python3-pip git && mkdir -p /root/.config/nvim
