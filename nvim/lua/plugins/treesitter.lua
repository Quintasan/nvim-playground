return {
	{
	    "RRethy/nvim-treesitter-endwise",
	    lazy = true
	},
  {
    "nvim-treesitter/nvim-treesitter",
    build = function()
      require("nvim-treesitter.install").update({with_sync = true})
    end,
    dependencies = {
	    "RRethy/nvim-treesitter-endwise"
    },
    config = function()
      require("nvim-treesitter.configs").setup {
	ensure_installed = { "ruby", "lua", "vim", "vimdoc" },

	highlight = {
	  enable = true,
	  additional_vim_regex_highlighting = false
	},
	    endwise = {
        enable = true,
    }
      }
      end
  }
}
