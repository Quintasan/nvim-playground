return {
  {
    "folke/tokyonight.nvim",
    lazy = false, 
    priority = 1000,
    config = function()
      vim.cmd([[colorscheme tokyonight]])
    end,
  },
  {
    "nvim-lualine/lualine.nvim",
    config = function()
      require("plugins.config.lualine")
    end,
    event = "VimEnter"
  },
  {
    "numToStr/Comment.nvim",
    lazy = false,
    opts = {
	       mappings = {
		       basic = false,
		       extra = false
	       }
    },
    init = function()
      vim.keymap.set('n', '<leader>cn', '<Plug>(comment_toggle_linewise_current)', {})
      vim.keymap.set('v', '<leader>cn', '<Plug>(comment_toggle_linewise_visual)', {})
    end
  },
  {
	  "mvllow/modes.nvim",
	  tag = "v0.2.1",
	  config = true
  }
}
