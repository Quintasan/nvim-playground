return {
  {
    "vim-test/vim-test",
    lazy = true,
    keys = {
      { '<leader>ta', '<cmd>TestSuite<cr>' },
      { '<leader>tf', '<cmd>TestFile<cr>' },
      { '<leader>tc', '<cmd>TestNearest<cr>' },
      { '<leader>tl', '<cmd>TestLast<cr>' },
    }
  }
}
