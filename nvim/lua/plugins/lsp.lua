return {
  { "nvim-lua/lsp-status.nvim" },

	{ "williamboman/mason.nvim",
	  dependencies = {
	    "williamboman/mason-lspconfig.nvim",
	    "neovim/nvim-lspconfig"
	  },
	  config = function()
	    require("lsp").setup()
	  end,
	  build = ":MasonUpdate"
	},

  {
    "j-hui/fidget.nvim",
    config = function() require("plugins.config.fidget") end,
    event = "LspAttach",
    tag = "legacy",
  },

  {
    "folke/trouble.nvim",
    dependencies = {
      "kyazdani42/nvim-web-devicons",
    },
    config = function()
      require("plugins.config.trouble")
    end,
    event = "BufEnter"
  }
}
