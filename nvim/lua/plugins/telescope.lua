return {
  {
    'nvim-telescope/telescope.nvim',
    lazy = false,
    tag = '0.1.2',
    dependencies = { 'nvim-lua/plenary.nvim' },
    config = function()
      require("plugins.config.telescope")
    end,
    event = "BufEnter"
  },
  { 
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
    init = function()
      require('telescope').load_extension('fzf')
    end
  }
}
