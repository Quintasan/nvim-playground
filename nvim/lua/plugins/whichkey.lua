return {
  {
    "folke/which-key.nvim",
    event = "VimEnter",
    config = function()
      require("plugins.config.whichkey").setup()
    end
  }
}
