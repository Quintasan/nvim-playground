local M = {}

M.setup = function()
  M.load_plugins()

  local defaults = {
	  on_attach = M.default_on_attach(),
	  capabilities = M.set_capabilities(),
  }

  M.setup_language_servers(defaults)
end

M.load_plugins = function()
  local loaded, mason = pcall(require, "mason")
  if not loaded then
    return
  end

  local loaded, mlsp = pcall(require, "mason-lspconfig")
  if not loaded then
    return
  end

  mason.setup()
  mlsp.setup()
end

M.default_on_attach = function(client, bufnr)
  require("lsp.keymaps").setup(client, bufnr)
end

M.set_capabilities = function()
  local capabilities = vim.lsp.protocol.make_client_capabilities()

  local loaded, cmp_nvim_lsp = pcall(require, "cmp_nvim_lsp")
  if not loaded then
    return capabilities
  end

  return cmp_nvim_lsp.default_capabilities(capabilities)
end

M.setup_language_servers = function(defaults)
  local lspconfig = require('lspconfig')

  lspconfig.solargraph.setup(defaults)
end

return M
